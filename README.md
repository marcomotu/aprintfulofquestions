# aPrintfulOfQuestions

application for front-end developer position @Printful

## Structure

A simple system with a test, where an user can enter their name, choose a test, and see their results.


## Front-End

The user interface is built as a Single Page Application to make the navigation friendly and fast.

It has three views:  

1.   Intro, where the users input their name and select one of the available tests    
2.   Test, where the user chooses one of the available answer    
3.   Outro where the results of the tests are displayed    

The interface is built using HTML, CSS and JS (jQuery).


## Back-end
Is created using a Google Sheet as database, 
https://docs.google.com/spreadsheets/d/1ADlUnzUgn5TMpnVipCr5K24xClE2IeR6WikydDp1JO8/edit?usp=sharing

and a Google Apps Script to serve it
https://script.google.com/d/1FwM6eD1Z29QeZ-8jeCI9dTNyS_PupecjJtJCReLZb1ojsCz36gokMPiD/edit?usp=sharing

In the Google Sheet:  

*    every Sheet corresponds to a Test,     
*  every Row corresponds to a question, where the first column is the question's text   
*  every Cell in a Column, starting from the second, is an answer. If a cell's text is in bold font, the answer is correct  